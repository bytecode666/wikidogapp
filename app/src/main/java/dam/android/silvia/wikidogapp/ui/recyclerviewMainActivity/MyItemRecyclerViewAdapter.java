package dam.android.silvia.wikidogapp.ui.recyclerviewMainActivity;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import dam.android.silvia.wikidogapp.Modelo.ModeloPerro;
import dam.android.silvia.wikidogapp.R;

import java.util.List;

public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {


    public interface OnItemClickListener {
        void onItemClick(ModeloPerro item);
    }

    private OnItemClickListener listener;
    private final List<ModeloPerro> mValues;

    public MyItemRecyclerViewAdapter(List<ModeloPerro> items, OnItemClickListener listener) {
        mValues = items;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardviewperros, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.bind(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView raza;
        TextView detallePers;
        ImageView imagen;

        public ViewHolder(View view) {
            super(view);
            this.mView = view;
            raza = view.findViewById(R.id.cvTvNombreRaza);
            detallePers = view.findViewById(R.id.cvTvDescripcionPers);
            imagen = view.findViewById(R.id.imgPerro);
        }

        public void bind(ModeloPerro item) {
            Picasso.get().load(item.getUrlImgRaza()).into(imagen);
            this.raza.setText(item.getRaza());
            this.detallePers.setText(item.getPersonalidad());

            mView.setOnClickListener(v -> listener.onItemClick(item));
        }
    }
}