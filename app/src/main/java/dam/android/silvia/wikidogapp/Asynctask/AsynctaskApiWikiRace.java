package dam.android.silvia.wikidogapp.Asynktask;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import dam.android.silvia.wikidogapp.Modelo.ModeloPerro;
import dam.android.silvia.wikidogapp.ui.recyclerviewMainActivity.RecyclerviewContent;

public class AsynctaskApiWikiRace extends AsyncTask<URL, Void, ArrayList<ModeloPerro>> {
    private final int CONNECTION_TIMEOUT = 15000;
    private final int READ_TIMEOUT = 10000;

    private ArrayList<ModeloPerro> searchResult;

    @SuppressLint("WrongThread")
    @Override
    protected ArrayList<ModeloPerro> doInBackground(URL... urls) {
        HttpURLConnection urlConnection = null;
        searchResult = new ArrayList<>();

        try {

            urlConnection = (HttpURLConnection) urls[0].openConnection();
            urlConnection.setRequestProperty("Connection", "Close");
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT);
            urlConnection.setReadTimeout(READ_TIMEOUT);

            if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                String resultStream = readStream(urlConnection.getInputStream());
                JSONObject json = new JSONObject(resultStream);
                JSONArray jArray = json.getJSONArray("razas");
                if (jArray.length() > 0) {

                    for (int i = 0; i < jArray.length(); i++) {

                        JSONObject item = jArray.getJSONObject(i);
                        JSONArray langs = item.getJSONArray("langs");
                        String name = item.getString("name");
                        String descripcion = "";
                        String personalidad = "";
                        String url = item.getString("img");
                        String cuidados = "";
                        String comportamiento = "";
                        String agresividad = item.getString("agresividad");

                        for (int j = 0; j < langs.length(); j++) {

                            JSONObject langItem = langs.getJSONObject(j);

                            // Coge los items con lenguaje español del JSON
                            if (langItem.getString("iso_code").equals("es")) {

                                name = langItem.getString("name");
                                descripcion = langItem.getString("description");

                                personalidad = langItem.getString("personalidad");

                                if(personalidad.isEmpty()){
                                    personalidad = langItem.getString("resume");
                                }

                                comportamiento = langItem.getString("cuidados");
                                cuidados = langItem.getString("comportamiento");
                            }
                        }
                        searchResult.add(new ModeloPerro(name, descripcion, personalidad, url, cuidados, comportamiento, agresividad));
                        if (isCancelled()) break;
                    }
                }

            } else {
                Log.i("URL", "ErrorCode: " + urlConnection.getResponseCode());
            }
        } catch (IOException ex) {
            Log.i("IOException", ex.getMessage());
        } catch (JSONException ex) {
            Log.i("JSONException", ex.getMessage());

        } finally {
            if (urlConnection != null) urlConnection.disconnect();
        }

        return searchResult;
    }

    // Añade al arraylist del recyclerview los JSON Objects de la API
    @Override
    protected void onPostExecute(ArrayList<ModeloPerro> searchResult) {

        if (searchResult != null && searchResult.size() > 0) {
            for (ModeloPerro item : searchResult) {
                RecyclerviewContent.addItem(item);
            }
        }
    }

    private String readStream(InputStream in) {

        StringBuilder sb = new StringBuilder();

        try {

            BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            String nextLine = "";

            while ((nextLine = reader.readLine()) != null) {
                sb.append(nextLine);
            }

        } catch (IOException ex) {
            Log.i("IOException-ReadStream", ex.getMessage());
        }
        return sb.toString();
    }

    // Cancela la asynctask si ha sido cancelada
    @Override
    protected void onCancelled() {
        super.onCancelled();
        Log.e("onCancelled()", "ASYNCTASK wikirace: I've been cancelled and ready to GC clean");
    }
}