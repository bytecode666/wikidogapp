package dam.android.silvia.wikidogapp;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

import dam.android.silvia.wikidogapp.Asynktask.AsynktaskApiWikiRace;

import static dam.android.silvia.wikidogapp.MainActivity.URL_API;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int TIME_TO_SLEEP = 2000;
    private AsynktaskApiWikiRace asynktaskApiWikiRace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ejecutarAsynkTask();
        // Activity sin la barra de notificaciones, a pantalla completa.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        // Esconde el ActionBar
        ActionBar ab = getSupportActionBar();
        if (ab != null)
            ab.hide();

        Thread timer = new Thread() {

            @Override
            public void run() {
                super.run();
                try {
                    if (asynktaskApiWikiRace != null) {

                        do {
                            sleep(TIME_TO_SLEEP);
                        } while (!asynktaskApiWikiRace.getStatus().equals(AsyncTask.Status.FINISHED));

                        startActivity(new Intent("WikiDogApp.MAINACTIVITY"));
                    }
                } catch (InterruptedException ex) {
                }
            }
        };

        timer.start();
    }

    public void ejecutarAsynkTask() {
        URL url;
        try {
            if (isNetworkAvailable()) {
                url = new URL(URL_API);
                asynktaskApiWikiRace = new AsynktaskApiWikiRace();
                asynktaskApiWikiRace.execute(url);

            } else {
                Toast.makeText(this, "Necesito una conexión de red para funcionar", Toast.LENGTH_LONG).show();
                finish();
            }

        } catch (MalformedURLException e) {
            Log.e("URL", e.getMessage());
        }
    }

    public boolean isNetworkAvailable() {

        boolean networkAvailable = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                networkAvailable = true;
            }
        }

        return networkAvailable;
    }

    @Override
    public void onPause() {
        super.onPause();
        finish();
    }
}