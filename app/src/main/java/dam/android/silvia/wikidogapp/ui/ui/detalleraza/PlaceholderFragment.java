package dam.android.silvia.wikidogapp.ui.ui.detalleraza;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import dam.android.silvia.wikidogapp.Modelo.ModeloPerro;
import dam.android.silvia.wikidogapp.R;
import dam.android.silvia.wikidogapp.ui.DetalleRazaActivity;

public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;
    private int index;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        index = 1;
        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = cambiarFragmentLayout(index, inflater, container);
        return root;
    }

    // Metodo que infla un fragment u otro segun tab elegido
    public View cambiarFragmentLayout(int index, LayoutInflater inflater, ViewGroup container) {

        View root = null;
        TextView textview;
        ModeloPerro item = ((DetalleRazaActivity) getActivity()).getItem();
        switch (index) {

            case 0:
                root = inflater.inflate(R.layout.fragment_info_raza, container, false);
                textview = root.findViewById(R.id.tvDescripcion);
                TextView tvAgresividad = root.findViewById(R.id.tvAgresividad);
                textview.setText(item.getDescripcion());
                tvAgresividad.setText(getString(R.string.agresividad) + " " + item.getAgresividad());
                break;

            case 1:
                root = inflater.inflate(R.layout.fragment_comportamiento_raza, container, false);
                textview = root.findViewById(R.id.tvComportamiento);
                textview.setText(item.getComportamiento());
                break;

            case 2:
                root = inflater.inflate(R.layout.fragment_cuidado_raza, container, false);
                textview = root.findViewById(R.id.tvCuidados);
                textview.setText(item.getCuidados());
                break;
        }

        return root;
    }
}