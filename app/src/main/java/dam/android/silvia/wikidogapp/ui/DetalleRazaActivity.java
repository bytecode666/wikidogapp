package dam.android.silvia.wikidogapp.ui;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;

import dam.android.silvia.wikidogapp.Modelo.ModeloPerro;
import dam.android.silvia.wikidogapp.R;
import dam.android.silvia.wikidogapp.ui.ui.detalleraza.SectionsPagerAdapter;

import static dam.android.silvia.wikidogapp.ui.ItemFragment.KEY_BUNDLE;

public class DetalleRazaActivity extends AppCompatActivity {

    public ModeloPerro item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_raza);

        Serializable item = getIntent().getSerializableExtra(KEY_BUNDLE);
        this.item = (ModeloPerro) item;

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

    }

    public ModeloPerro getItem() {
        return item;

    }
}