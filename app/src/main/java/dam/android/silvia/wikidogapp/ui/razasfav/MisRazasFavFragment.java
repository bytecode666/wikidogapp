package dam.android.silvia.wikidogapp.ui.razasfav;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import dam.android.silvia.wikidogapp.R;

public class MisRazasFavFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_misrazasfav, container, false);
        return root;
    }
}