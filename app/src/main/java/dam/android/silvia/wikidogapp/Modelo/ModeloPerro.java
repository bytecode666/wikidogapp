package dam.android.silvia.wikidogapp.Modelo;

import java.io.Serializable;

public class ModeloPerro implements Serializable {

    private String raza;
    private String descripcion;
    private String personalidad;
    private String cuidados;
    private String comportamiento;
    private String agresividad;
    private String urlImgRaza;

    public ModeloPerro(String raza, String descripcion, String personalidad, String urlImgRaza, String cuidados, String comportamiento, String agresividad) {
        this.raza = raza;
        this.descripcion = descripcion;
        this.personalidad = personalidad;
        this.urlImgRaza = urlImgRaza;
        this.cuidados = cuidados;
        this.comportamiento = comportamiento;
        this.agresividad = agresividad;
    }

    public String getRaza() {
        return this.raza;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public String getCuidados() {
        return this.cuidados;
    }

    public String getAgresividad() {
        return this.agresividad;
    }

    public String getComportamiento() {
        return this.comportamiento;
    }

    public String getPersonalidad() {
        return this.personalidad;
    }

    public String getUrlImgRaza() {
        return this.urlImgRaza;
    }

    @Override
    public String toString() {
        return raza + " " + personalidad;
    }
}